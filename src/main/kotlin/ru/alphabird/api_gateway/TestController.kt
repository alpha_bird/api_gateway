package ru.alphabird.api_gateway

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController("/test")
class TestController {
    private val logger: Logger = LoggerFactory.getLogger(this::class.java)

    @GetMapping("/hello")
    fun getHello(): String {
        logger.info("Get hello controller is executed")
        return "Hey Hello"
    }
}
