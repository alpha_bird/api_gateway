package ru.alphabird.api_gateway

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ApiGatewayApplication

private val logger: Logger = LoggerFactory.getLogger(ApiGatewayApplication::class.java)

fun main(args: Array<String>) {
	logger.info("APPLICATION IS READY TO RUN")
	runApplication<ApiGatewayApplication>(*args)
}
